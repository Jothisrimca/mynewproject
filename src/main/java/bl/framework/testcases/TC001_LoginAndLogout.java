package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import bi.framework.design.ProjectMethods;
import bl.framework.api.SeleniumBase;

public class TC001_LoginAndLogout extends ProjectMethods {

	@Test
	public void login1() {
		
		 
		WebElement lead = locateElement("linktext", "Create Lead");
		click(lead);
		WebElement compN = locateElement("id", "createLeadForm_companyName");
		clearAndType(compN,"TechMahindra");
	    WebElement fName = locateElement("id","createLeadForm_firstName");
	    clearAndType(fName,"Jothisri");
	    WebElement lName = locateElement("id", "createLeadForm_lastName");
	    clearAndType(lName, "Kumar");
	    WebElement LeadClick = locateElement("class", "smallSubmit");
	    click(LeadClick);
	    WebElement editLead = locateElement("class","subMenuButton");
	    click(editLead);
	    WebElement fNameLocal = locateElement("id", "createLeadForm_firstNameLocal");
	    clearAndType(fNameLocal, "JothisriKumar");
	    WebElement lead1 = locateElement("class", "smallSubmit");
		click(lead1);
	   
	}
}








