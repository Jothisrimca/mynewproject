package Util;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class ReadExcel {
	static Object[][] data;
    @Test 
	public static Object[][] readData(String dataSheetName) {
		// TODO Auto-generated method stub
		
		//Enter WorkBook(locate file)
		XSSFWorkbook wbook;
		try {
			wbook = new XSSFWorkbook("./DATA/+dataSheetName+.xlsx");
		
		//Enter Sheet 
		XSSFSheet sheet = wbook.getSheet("Sheet1"); 
		//Getting row count 
		int rowCount = sheet.getLastRowNum();
		System.out.println("Row Count"+rowCount);
		//Getting cell count for header 
		int colcount = sheet.getRow(0).getLastCellNum();
		System.out.println("Column count"+colcount);
		
		data = new Object[rowCount][colcount];
		for(int i = 1; i<=rowCount;i++) {
			XSSFRow row = sheet.getRow(i); //Entering row
			for(int j = 0; j<colcount; j++) {//Entering cell
				XSSFCell cell = row.getCell(j);//getting value from cell
				data[i-1][j] = cell.getStringCellValue();
				
			}
			
		}
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;	

	}
	
	

}
